<?php

/**
 * Admin form
 */
function selectmenu_admin_form($form_state) {
	$form = array();
	$form['selectmenu_enabled'] = array(
		'#type' => 'checkbox',
		'#title' => t('Enable jQuery UI seletmenu'),
		'#default_value' => variable_get('selectmenu_enabled', TRUE),
		'#description' => t('Use this option to toggle jQuery UI selectmenu on and off globally.'),
	);
	$form['selectmenu_form_id_exceptions'] = array(
		'#type' => 'textarea',
		'#title' => t('Form CSS IDs to ignore'),
		'#description' => t('Enter the CSS IDs of the forms to not enable jQuery UI selectmenu on. One per line.'),
		'#rows' => 3,
		'#cols' => 40,
		'#default_value' => variable_get('selectmenu_form_id_exceptions', ''),
	);
	$form['selectmenu_ignore_system_settings_forms'] = array(
		'#type' => 'checkbox',
		'#title' => t('Ignore system settings forms'),
		'#description' => t('Do not use jQuery UI selectmenu for Drupal system settings form.'),
		'#default_value' => variable_get('selectmenu_ignore_system_settings_forms', TRUE),
	);

	return system_settings_form($form);
}